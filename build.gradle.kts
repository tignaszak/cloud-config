import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.springframework.boot") version "2.7.5"
    id("io.spring.dependency-management") version "1.1.0"
    kotlin("jvm") version "1.7.20"
    kotlin("plugin.spring") version "1.7.20"
}

group = "net.ignaszak"
version = "1.0.0"

java.sourceCompatibility = JavaVersion.VERSION_18
java.targetCompatibility = JavaVersion.VERSION_18

repositories {
    mavenCentral()
    maven { url = uri("https://repo.spring.io/milestone") }
}

tasks.getByName<Jar>("jar") {
    enabled = false
}

dependencies {
    implementation("org.yaml:snakeyaml:1.33")
    implementation("com.fasterxml.woodstox:woodstox-core:6.4.0")
    implementation("com.google.guava:guava:31.1-jre")
    implementation("com.thoughtworks.xstream:xstream:1.4.19")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.springframework.cloud:spring-cloud-config-server:3.1.4")
    implementation("org.springframework.cloud:spring-cloud-starter-config:3.1.4")
    implementation("org.springframework.cloud:spring-cloud-starter-netflix-eureka-client:3.1.4")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "18"
    }
}